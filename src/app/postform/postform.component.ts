import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PostsService } from './../posts.service';

@Component({
  selector: 'app-postform',
  templateUrl: './postform.component.html',
  styleUrls: ['./postform.component.css']
})
export class PostformComponent implements OnInit {

  constructor(private postsservice:PostsService, private router:Router, private route:ActivatedRoute) { }


  name:string;
  body:string;
  author:string;
  id:string; 
  isEdit:boolean = false;
  buttonText:string = 'Add post'
  
  onSubmit(){ 
    if(this.isEdit){
      console.log('edit mode');
      this.postsservice.updatePost(this.id,this.name,this.author,this.body);
    } else {
      this.postsservice.addPost(this.name,this.author,this.body)
    }
    this.router.navigate(['/posts']);  
  }  


  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    console.log(this.id);
    if(this.id) {
      this.isEdit = true;
      this.buttonText = 'Update Post'   
      this.postsservice.getPost(this.id).subscribe(
        post => {
          console.log(post.data().author)
          console.log(post.data().name)
          this.name = post.data().name; 
          this.author = post.data().author;
          this.body = post.data().body; 
          
        }
      )
     }
  }

}
