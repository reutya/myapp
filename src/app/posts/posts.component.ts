import { Component, OnInit } from '@angular/core';
import { Posts } from './../interfaces/posts';
import { Observable } from 'rxjs';
import { PostsService } from './../posts.service';
import { ActivatedRoute } from '@angular/router';
import { Users } from '../interfaces/users';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  panelOpenState = false;
 // posts$:any;
  //posts$: Posts[];
  //users$: Users[];
  // title:string;
  // body:string;
  // author:string;
  // ans:string;
  
  
  posts$:Observable<any[]>;
  //posts:any;

  constructor(private route: ActivatedRoute,private PostsService: PostsService) { }
  
  deletePost(id:string){
    this.PostsService.deletePost(id);
  }


  // myFunc(){
  //   for (let index = 0; index < this.posts$.length; index++) {
  //     for (let i = 0; i < this.users$.length; i++) {
  //       if (this.posts$[index].userId==this.users$[i].id) {
  //         this.title = this.posts$[index].title;
  //         this.body = this.posts$[index].body;
  //         this.author = this.users$[i].name;
  //         this.PostsService.addPosts(this.body,this.author,this.title);
          
  //       }
        
        
  //     }
      
  //   }
  //   this.ans ="The data retention was successful"
  // }

  ngOnInit() {

    this.posts$=this.PostsService.getPosts();
  
  }


  // ngOnInit() {תרגיל 8 ואחורה
  //    this.PostsService.getPosts()
  //     .subscribe(posts => this.posts$ = posts);

  //     this.PostsService.getUsers()
  //      .subscribe(posts => this.users$ = posts);
  // }

  // ngOnInit() {
  //   this.posts$ = this.PostsService.getPosts();
  //   this.posts$.subscribe(data => {this.posts = data;
  //       console.log(this.posts);
  //     }
  //   ) 
  // }

}
