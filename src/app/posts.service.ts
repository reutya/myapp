import { Injectable } from '@angular/core';
import { HttpClient,HttpClientModule  } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Posts } from './interfaces/posts';
import { Users } from './interfaces/users';
import { AngularFirestore } from '@angular/fire/firestore';
import { getMultipleValuesInSingleSelectionError } from '@angular/cdk/collections';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  // apiUrl = "https://jsonplaceholder.typicode.com/posts/";
  // apiUrlUsers="https://jsonplaceholder.typicode.com/users/";
  
  constructor(private http:HttpClient,private db:AngularFirestore) { }
  
  
  //קשור לאינטרפייס ולששAPI getPosts(){    
  //   return this.http.get<Posts[]>(this.apiUrl);
  // }

  // getUsers(){    
  //   return this.http.get<Users[]>(this.apiUrlUsers);
  // }
  
  getPosts():Observable<any[]>{
    return this.db.collection('posts').valueChanges({idField:'id'});
   }


  getPost(id:string):Observable<any>{
    return this.db.doc(`posts/${id}`).get();
   }
  
  addPost(body:string, author:string,name:string){
    const post= {body:body, author:author, name:name};
    this.db.collection('posts').add(post);
  }
  

  addPosts(body:string, author:string,name:string){
    const post = {body:body, author:author, name:name};
    this.db.collection('posts').add(post);
  }

  deletePost(id:string){
    this.db.doc(`posts/${id}`).delete();
  }
  
  updatePost(id:string,body:string, author:string,name:string){
    this.db.doc(`posts/${id}`).update({body:body, author:author, name:name})
  
  }

}





