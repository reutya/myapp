import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-editauthor',
  templateUrl: './editauthor.component.html',
  styleUrls: ['./editauthor.component.css']
})
export class EditauthorComponent implements OnInit {

  name:string;
  id;
  

  onSubmit(){
    this.router.navigate(['/authors', this.id,this.name]);
  }

  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.id= this.route.snapshot.params.id;
  }

}