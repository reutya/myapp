import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Router } from "@angular/router";
import { Location } from "@angular/common";
import { AuthService } from './../auth.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent {
  title: string = 'Books';

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  // constructor(private breakpointObserver: BreakpointObserver, location: Location, router: Router) {
  //   router.events.subscribe(val => {
  //     if (location.path() == "/books") {
  //       this.title = 'Books';
  //       console.log(val);        
  //     } else if(location.path() == "/authors") {
  //       this.title = "Authors";
  //     }
  //     else if(location.path() == "/posts") {
  //       this.title = "Posts";
  //     }
  //   });   
  // }


  constructor(private breakpointObserver: BreakpointObserver, 
    public authService:AuthService,
    location: Location, 
    router: Router){
router.events.subscribe(val => {
if (location.path() == "/books") {
this.title = 'Books';      
} else if (location.path() == "/signup"){
this.title = "Sign up form";
} else if(location.path() == "/authors") {
        this.title = "Authors";
}else if(location.path() == "/posts") {
       this.title = "Posts";
}
});   
}

}
