// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {

    apiKey: "AIzaSyC11zlGfCF9P69XaThgIhkbApFTtVJlsiE",
    authDomain: "my-app-b3a40.firebaseapp.com",
    databaseURL: "https://my-app-b3a40.firebaseio.com",
    projectId: "my-app-b3a40",
    storageBucket: "my-app-b3a40.appspot.com",
    messagingSenderId: "393286672922",
    appId: "1:393286672922:web:be5277bfd0476028b6a7a3",
    measurementId: "G-4VSSPE6F4T"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
